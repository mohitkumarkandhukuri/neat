package com.aem.lab.core.servlets;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.inject.Singleton;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.lab.core.utils.PageUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

@Component(service=Servlet.class,
property={
        Constants.SERVICE_DESCRIPTION + "=Images in Page",
        "sling.servlet.methods=" + HttpConstants.METHOD_GET,
        "sling.servlet.paths="+ "/bin/neat/bannerImages",
        "sling.servlet.resourceTypes="+ "neat/components/structure/page",
        "sling.servlet.selectors="+ "image",
        "sling.servlet.extensions=" + "json"
})
public class ResourceToJsonServlet extends SlingAllMethodsServlet{
	private static final long serialVersionUID = 1L;
	private Logger log = LoggerFactory.getLogger(ResourceToJsonServlet.class);
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		ResourceResolver resolver = request.getResourceResolver();
		PageManager pageManager = resolver.adaptTo(PageManager.class);
		Resource resource = request.getResource();
		log.debug("\n\t This Resource : " + resource.getPath() + "\n");
		Page currentPage = pageManager.getContainingPage(resource);
		log.debug("\n\t This Page : " + currentPage.getPath() + "\n");
		Page homePage = PageUtils.getHomePage(currentPage);
		log.debug("\n\t Home Page : " + homePage.getPath() + "\n");
		JSONArray jsonArray = new JSONArray();
		parseJsonObj(homePage, jsonArray);
		log.debug("\n\t Parsed JSON Object : " + jsonArray + "\n");
		response.getWriter().print(jsonArray.toString());
	}

	private void parseJsonObj(Page page, JSONArray jsonArray) {
		JSONObject obj = new JSONObject();
		String pagePath = page.getPath();
		List<String> images = PageUtils.getBannerImagesInPage(page);
		try {
			if(!images.isEmpty()) {
				obj.put("pagePath",pagePath);
				obj.put("images", images);
				jsonArray.put(obj);
			}
		} catch (JSONException e) {
			log.debug("\n\t Exception : " + e.getMessage() + "\n");
		}
		Iterator<Page> childItr = page.listChildren();
		while (childItr.hasNext()) {
			Page childPage = (Page) childItr.next();
			parseJsonObj(childPage, jsonArray);
		}
	}

}
