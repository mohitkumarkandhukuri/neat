package com.aem.lab.core.models;

public interface PromoHero {

	String getIcon();

	String getTitle();

	String getDesc();

}
