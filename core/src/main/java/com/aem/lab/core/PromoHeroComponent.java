package com.aem.lab.core;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.aem.lab.core.models.PromoHero;

@Model(adaptables = Resource.class, adapters = {PromoHero.class, PromoHeroComponent.class},
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class PromoHeroComponent implements PromoHero{
	
	@Inject
	private String icon;
	
	@Inject
	private String title;
	
	@Inject
	private String desc;

	@Override
	public String getIcon() {
		return icon;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getDesc() {
		return desc;
	}

}
