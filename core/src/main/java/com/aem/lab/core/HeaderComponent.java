package com.aem.lab.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.lab.core.models.Header;
import com.aem.lab.core.utils.PageUtils;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.common.collect.Lists;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, adapters = { Header.class,
		HeaderComponent.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeaderComponent implements Header {

	private Logger log = LoggerFactory.getLogger(HeaderComponent.class);

	@Inject
	@Via("resource")
	private Resource resource;

	@Inject
	@Via("resource")
	private String rootPath;

	@Inject
	private Page currentPage;

	private List<Page> childPages;

	@PostConstruct
	public void postConstruct() {
		childPages = new ArrayList<>();
		log.debug(" current Page : " + currentPage.getPath());
		childPages = PageUtils.getHomePageChildren(currentPage);
		if (rootPath == null) {
			rootPath = PageUtils.getHomePageLink(currentPage);
		}
	}
	
	@Override
	public String getActiveHirearchy() {
		return PageUtils.getActiveHirearchy(currentPage);
	}

	@Override
	public String getRootPath() {
		return rootPath;
	}

	@Override
	public List<Page> getChildPages() {
		return childPages;
	}

}
