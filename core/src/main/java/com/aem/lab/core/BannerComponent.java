package com.aem.lab.core;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.aem.lab.core.models.Banner;

@Model(adaptables = Resource.class, adapters = {Banner.class, BannerComponent.class},
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class BannerComponent implements Banner{
	
	@Inject
	private Resource resource;
	
	@Inject
	private String imageSrc;
	
	@Inject
	private String title;
	
	@Inject
	private String desc;
	
	@Inject
	private String position;
	
	@Inject
	private List<ButtonComponent> buttons;

	@Override
	public String getImageSrc() {
		return imageSrc;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getDesc() {
		return desc;
	}

	@Override
	public String getPosition() {
		return position;
	}
	
	@Override
	public List<ButtonComponent> getButtons() {
		return buttons;
	}
	

}
