package com.aem.lab.core;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.lab.core.models.Footer;
import com.aem.lab.core.models.Header;
import com.aem.lab.core.utils.PageUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

@Model(adaptables = SlingHttpServletRequest.class, adapters = { Footer.class,
		FooterComponent.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FooterComponent implements Footer {

	private static Logger log = LoggerFactory.getLogger(FooterComponent.class);

	@Inject
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private Page currentPage;
	
	@Inject
	@Via("resource")
	private TextComponent about;
	
	@Inject
	@Via("resource")
	private String copyRightText;

	@Inject
	@Via("resource")
	private List<ButtonComponent> posts;

	@Inject
	@Via("resource")
	private TextComponent contact;

	private String rootPath;
	private List<Page> childPages;
	private String year;
	private Resource footer;
	private ValueMap footerProp;

	@PostConstruct
	public void postConstruct() {
		if(currentPage != null ) {
			rootPath = PageUtils.getHomePageLink(currentPage);
			childPages = PageUtils.getHomePageChildren(currentPage);
		}
		footer = PageUtils.getHomePage(currentPage).getContentResource("root/footer");
		footerProp = footer.getValueMap();
		if(posts == null) {
			List<ButtonComponent> posts = new ArrayList<>();
			footer.getChild("posts").getChildren().forEach(r -> posts.add(r.adaptTo(ButtonComponent.class)));
			this.posts = posts;
		}
		Calendar calendar = Calendar.getInstance();
		year = Integer.toString(calendar.get(Calendar.YEAR));
	}
	
	@Override
	public String getActiveHirearchy() {
		return PageUtils.getActiveHirearchy(currentPage);
	}
	
	@Override
	public String getYear() {
		return year;
	}
	
	@Override
	public String getCopyRightText() {
		return copyRightText != null ? copyRightText : (String) footerProp.get("copyRightText");
	}

	@Override
	public String getRootPath() {
		return rootPath != null ? rootPath + ".html" : (String) footerProp.get("rootPath") + ".html";
	}

	@Override
	public List<Page> getChildPages() {
		return childPages;
	}

	@Override
	public TextComponent getAbout() {
		return about != null ? about : footer.getChild("about").adaptTo(TextComponent.class);
	}

	@Override
	public List<ButtonComponent> getPosts() {
		return posts;
	}

	@Override
	public TextComponent getContact() {
		return contact != null ? contact : footer.getChild("contact").adaptTo(TextComponent.class);
	}

}
