package com.aem.lab.core;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = Resource.class)
public class ImageComponent {
	
	@Inject
	private String imgPath;
	
	@Inject
	private String title;
	
	@Inject
	private String desc;
	
	@Inject
	private String path;
	
	public String getPath() {
		return path;
	}

	public String getImgPath() {
		return imgPath;
	}

	public String getTitle() {
		return title;
	}

	public String getDesc() {
		return desc;
	}

}
