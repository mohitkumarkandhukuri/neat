package com.aem.lab.core;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.aem.lab.core.models.ImageGallery;

@Model(adaptables = Resource.class, adapters = {ImageGallery.class, ImageGalleryComponent.class},
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ImageGalleryComponent implements ImageGallery {
	
	@Inject
	private List<ImageComponent> images;
	
	@Override
	public List<ImageComponent> getImages() {
		return images;
	}

}
