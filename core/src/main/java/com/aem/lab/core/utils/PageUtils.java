package com.aem.lab.core.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.lab.core.FooterComponent;
import com.day.cq.wcm.api.Page;
import com.google.common.collect.Lists;

public class PageUtils {

	private static Logger log = LoggerFactory.getLogger(PageUtils.class);
	private static final String HOME_PAGE = "/conf/neat/settings/wcm/templates/neat-home-page";

	public static boolean isHomePage (Page page) {
		return page.getProperties().get("cq:template", String.class).equals(HOME_PAGE);
	}
	
	public static Page getHomePage(Page page) {
		while (page != null) {
			if (isHomePage(page)) {
				log.debug(" home page : " + page.getPath());
				return page;
			}
			page = page.getParent();
		}
		return null;
	}
	
	public static String getActiveHirearchy(Page page) {
		if (page != null) {
			Page absParent = page.getAbsoluteParent(3);
			return absParent != null ? absParent.getPath() : page.getPath();
		}
		return null;
	}

	public static List<Page> getHomePageChildren(Page page) {
		if (page != null) {
			Page homePage = getHomePage(page);
			if (homePage != null) {
				return Lists.newArrayList(homePage.listChildren());
			}
		}
		return new ArrayList<>();
	}

	public static String getHomePageLink(Page page) {
		Page homePage = getHomePage(page);
		Resource header = homePage.getContentResource("root/header");
		if (header != null) {
			return header.getValueMap().get("rootPath", String.class);
		}
		return page.getPath();
	}

	public static List<String> getBannerImagesInPage(Page page) {
		Resource contentResource = page.getContentResource();
		if(contentResource.hasChildren()) {
			if (isHomePage(page)) {
				log.debug("\n\t Home Page \n");
				Resource par = contentResource.getChild("root/responsivegrid");
				return getImagesFromResource(par.listChildren());
			} else {
				log.debug("\n\t Not home page \n");
				Resource par = contentResource.getChild("root");
				return getImagesFromResource(par.listChildren());
			}
		}
		return new ArrayList<>();
	}
	
	private static List<String> getImagesFromResource(Iterator<Resource> itr){
		List<String> imageList = new ArrayList<>();
		itr.forEachRemaining(r -> {
			if (r.getResourceType().equals("neat/components/content/banner-component")) {
				String imageSrc = r.getValueMap().get("imageSrc", String.class);
				imageList.add(imageSrc);
			}
		});
		log.debug("\n\t Images in Page : " + imageList + "\n");
		return imageList;
	}
}
