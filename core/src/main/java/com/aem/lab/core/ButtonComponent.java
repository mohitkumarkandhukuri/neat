package com.aem.lab.core;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = Resource.class)
public class ButtonComponent {
	
	@Inject
	private String text;
	
	@Inject
	private String path;

	public String getText() {
		return text;
	}

	public String getPath() {
		return path;
	}
	
	public boolean isExternal() {
		return path.startsWith("https") || path.startsWith("http");
	}

}
