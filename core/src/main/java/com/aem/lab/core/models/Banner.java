package com.aem.lab.core.models;

import java.util.List;

import com.aem.lab.core.ButtonComponent;

public interface Banner {

	String getImageSrc();

	String getTitle();

	String getDesc();

	String getPosition();

	List<ButtonComponent> getButtons();

}
