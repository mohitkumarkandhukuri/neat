package com.aem.lab.core.models;

import java.util.List;

import com.aem.lab.core.ImageComponent;

public interface ImageGallery {

	List<ImageComponent> getImages();

}
