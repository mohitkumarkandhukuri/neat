package com.aem.lab.core.models;

import java.util.List;

import com.day.cq.wcm.api.Page;

public interface Header {

	String getRootPath();

	List<Page> getChildPages();

	String getActiveHirearchy();

}
