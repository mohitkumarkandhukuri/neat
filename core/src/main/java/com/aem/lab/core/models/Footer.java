package com.aem.lab.core.models;

import java.util.List;

import com.aem.lab.core.ButtonComponent;
import com.aem.lab.core.TextComponent;
import com.day.cq.wcm.api.Page;

public interface Footer {

	String getRootPath();

	List<Page> getChildPages();

	TextComponent getAbout();

	List<ButtonComponent> getPosts();

	TextComponent getContact();

	String getYear();

	String getCopyRightText();

	String getActiveHirearchy();

}
